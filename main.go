package main

import (
	"context"
	"fmt"
	"sync"
	"time"
)

var all = &sync.WaitGroup{}

func main() {
	rootCtx, rootCancel := context.WithCancel(context.Background()) // рут /\ пустой контекст

	all.Add(1)
	go createImages(rootCtx, 3)

	go func() {
		time.Sleep(time.Second * 3)
		rootCancel() // условная ошибка
	}()

	all.Wait()
}

func createImages(parent context.Context, images int) {
	defer all.Done()
	ctx, cancel := context.WithCancel(parent)
	defer cancel()

	for i := 1; i <= images; i++ { // условное кол-во изображений
		all.Add(1)
		go processImage(ctx, i)
	}

	for {
		select {
		case <-parent.Done(): // родитель убит мы тоже умрем
			fmt.Println("не удалось создать все ваши изображения(сбой в сервисе)")
			return
		}
	}
}

func processImage(parent context.Context, imgID int) {
	defer all.Done()
	ctx, cancel := context.WithCancel(parent)
	defer cancel()

	fmt.Printf("изображение обработано id: %d\n", imgID)

	all.Add(1)
	go uploadToDB(ctx, imgID)
	for {
		select {
		case <-parent.Done(): // родитель убит мы тоже умрем
			return
		}
	}
}

func uploadToDB(parent context.Context, imgID int) {
	defer all.Done()
	fmt.Printf("изображение добавлено в таблицу бд id: %d\n", imgID)
	for {
		select {
		case <-parent.Done(): // родитель убит мы тоже умрем
			return
		}
	}
}
